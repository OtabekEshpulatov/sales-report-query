WITH WeeklySales AS (
  SELECT
    time_id,
    channel_id,
    SUM(amount_sold) AS weekly_amount
  FROM
    sh.sales
  WHERE
    EXTRACT(WEEK FROM time_id) IN (49, 50, 51)
      AND EXTRACT(YEAR FROM time_id) = 1999
  GROUP BY
    time_id, channel_id
),
CenteredAvg AS (
  SELECT
    time_id,
    channel_id,
    weekly_amount,
    LAG(weekly_amount) OVER (PARTITION BY channel_id ORDER BY time_id) AS prev_weekly_amount,
    LEAD(weekly_amount) OVER (PARTITION BY channel_id ORDER BY time_id) AS next_weekly_amount
  FROM
    WeeklySales
)
SELECT
  ws.time_id,
  ws.channel_id,
  ws.weekly_amount,
  SUM(ws.weekly_amount) OVER (PARTITION BY ws.channel_id ORDER BY ws.time_id) AS CUM_SUM,
  (ws.weekly_amount + COALESCE(ca.prev_weekly_amount, 0) + COALESCE(ca.next_weekly_amount, 0)) / 3 AS CENTERED_3_DAY_AVG
FROM
  WeeklySales ws
JOIN
  CenteredAvg ca ON ws.time_id = ca.time_id AND ws.channel_id = ca.channel_id
ORDER BY
  ws.time_id, ws.channel_id;
